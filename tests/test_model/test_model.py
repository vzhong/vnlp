import os
import shutil
import unittest
import torch
import tempfile
import json
import logging
import random
import numpy as np
from torch import nn
from torch.nn import functional as F
from vnlp.model.model import Model
from argparse import Namespace


class TestModel(unittest.TestCase):

    def setUp(self):
        torch.manual_seed(0)
        np.random.seed(0)
        random.seed(0)
        self.root = os.path.dirname(__file__)
        os.chdir(self.root)
        self.dexp = os.path.join(self.root, 'exp')
        self.args = Namespace(epoch=20, gpus=[], exp_root=self.dexp, early_stop='acc', name='myexp', module='mymodule', din=2, batch=2)
        self.m = Model.from_module(self.args.module, self.args, module_root='modules')
        self.dout = os.path.join(self.root, 'exp', self.args.module, 'myexp')
        if not os.path.isdir(self.dout):
            os.makedirs(self.dout)

    def tearDown(self):
        if os.path.isdir(self.dexp):
            shutil.rmtree(self.dexp)

    def test_args(self):
        self.assertIs(self.m.args, self.m.module.args)

    def test_module_instance(self):
        self.assertIs(self.m.module, self.m.module_instance)

    def test_dout(self):
        self.assertEquals(self.dout, self.m.dout)

    def test_run_train_pred(self):
        train = self.m.module.get_data(200)
        dev = self.m.module.get_data(20)
        self.m.run_train(train, dev, logging_level=logging.CRITICAL, verbose=False)
        self.assertTrue(os.path.isfile(os.path.join(self.dout, 'train.log')))
        train_preds = self.m.run_pred(train, verbose=False)
        self.assertGreater(train.compute_metrics(train_preds)['acc'], 0.90)
        dev_preds = self.m.run_pred(dev, verbose=False)
        self.assertGreater(dev.compute_metrics(dev_preds)['acc'], 0.90)
