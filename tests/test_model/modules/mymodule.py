import torch
import numpy as np
from torch import nn
from torch.nn import functional as F
from vnlp.model.module import BaseModule
from vnlp.data.datasplit import DataSplit


class MyDataSplit(DataSplit):

    def compute_metrics(self, preds):
        correct = [ex['y'] == p for ex, p in zip(self, preds)]
        return {'acc': np.mean(correct)}


class Module(BaseModule):

    def __init__(self, args):
        super().__init__(args)
        self.net = nn.Linear(2, 2)

    def forward(self, x):
        return self.net(x)

    def get_data(self, n):
        data = []
        for i in range(n):
            x = np.random.uniform(-1, 1, size=2)
            y = 1 if x.sum() > 0 else 0
            data.append({'x': x.tolist(), 'y': y})
        return MyDataSplit(data)

    def featurize(self, batch, device=None):
        x = [ex['x'] for ex in batch]
        return torch.Tensor(x, device=device),

    def extract_preds(self, out, batch):
        return out.max(1)[1].tolist()

    def compute_loss(self, out, batch):
        gt = torch.tensor([ex['y'] for ex in batch])
        return F.cross_entropy(out, gt)


