import unittest
import torch
import numpy as np
from vnlp.nn import functional


class TestFunctional(unittest.TestCase):

    def test_pad(self):
        seqs = [
            [1, 2, 3],
            [4],
        ]
        padded, lens = functional.pad(seqs,pad_value=10)
        self.assertEquals(
            [[1, 2, 3], [4, 10, 10]], padded.tolist()
        )
        self.assertEquals([3, 1], lens.tolist())
    
    def test_mask(self):
        lens = torch.tensor([2, 3])
        total_length = 4
        mask = functional.mask(lens, invert=False, total_length=total_length)
        self.assertEquals(
            [[1, 1, 0, 0], [1, 1, 1, 0]],
            mask.tolist()
        )
        mask = functional.mask(lens, invert=True, total_length=total_length)
        self.assertEquals(
            [[0, 0, 1, 1], [0, 0, 0, 1]],
            mask.tolist()
        )
    
    def test_mask_invalid_scores(self):
        scores = torch.Tensor([
            [1, 2, 3],
            [4, 5, 6],
        ])
        lens = torch.tensor([1, 2])
        masked = functional.mask_invalid_scores(scores, lens)
        self.assertEquals(
            [[1, -np.inf, -np.inf], [4, 5, -np.inf]], masked.tolist()
        )
