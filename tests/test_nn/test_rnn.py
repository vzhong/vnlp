import unittest
import torch
import numpy as np
from torch import nn
from vnlp.nn import rnn


class TestRNN(unittest.TestCase):

    def test_packed_rnn(self):
        inputs = torch.Tensor(np.random.uniform(0, 1, (2, 3, 1)))
        lens = torch.LongTensor([3, 2])
        for net in [nn.LSTM(1, 2, batch_first=True), nn.GRU(1, 2, batch_first=True)]:
            out, state = net(inputs)
            wrapped = rnn.PackedRNN(net)
            wrapped_out, wrapped_state = wrapped(inputs, lens)
            for i, l in enumerate(lens.tolist()):
                self.assertTrue(np.allclose(out[i, l-1].detach().numpy(), wrapped_out[i, l-1].detach().numpy()))

        for net in [nn.LSTM(1, 2, batch_first=False), nn.GRU(1, 2, batch_first=False)]:
            out, state = net(inputs.transpose(0, 1))
            wrapped = rnn.PackedRNN(net)
            wrapped_out, wrapped_state = wrapped(inputs.transpose(0, 1), lens)
            for i, l in enumerate(lens.tolist()):
                self.assertTrue(np.allclose(out[l-1, i].detach().numpy(), wrapped_out[l-1, i].detach().numpy()))
