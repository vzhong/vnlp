import unittest
import torch
import numpy as np
from vnlp.nn import attention


class TestAttention(unittest.TestCase):

    x = torch.Tensor(np.random.uniform(0, 1, size=(2, 3, 4)))
    lens = torch.tensor([1, 3])
    cond = torch.Tensor(np.random.uniform(0, 1, size=(2, 4)))

    def test_pointer_runnable(self):
        m = attention.Pointer()
        o = m(self.x, self.lens, self.cond)
        self.assertEquals(
            [2, 3], list(o.size())
        )

    def test_attention_runnable(self):
        m = attention.Attention(4)
        o = m(self.x, self.lens, self.cond)
        self.assertEqual([2, 4], list(o.size()))

    def test_self_attention_runnable(self):
        m = attention.SelfAttention(4)
        o = m(self.x, self.lens)
        self.assertEqual([2, 4], list(o.size()))

    def test_coattenion_runnable(self):
        d = self.x
        d_len = self.lens
        q = torch.Tensor(np.random.uniform(0, 1, size=(2, 5, 4)))
        q_len = torch.tensor([2, 1])
        m = attention.Coattention(4)
        cd, sq, sd = m(q, q_len, d, d_len)
        self.assertEqual([2, 3, 4], list(cd.size()))
        self.assertEqual([2, 3, 4], list(sd.size()))
        self.assertEqual([2, 5, 4], list(sq.size()))
