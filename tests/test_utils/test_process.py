import unittest
import os
import tempfile
from vnlp.utils import process


class TestProcess(unittest.TestCase):

    def test_run(self):
        self.assertEquals('foobar', process.run(['echo', 'foobar']))
        self.assertIsNone(process.run(['echo', 'foobar'], only_if=False))
        self.assertRaises(Exception, lambda: process.run(['exit', '1'], fail_on_error=True))
        self.assertIsNone(process.run(['exit', '1'], fail_on_error=False))
