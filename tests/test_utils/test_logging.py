import unittest
import os
import tempfile
from vnlp.utils import logging
import logging as orig_logging


class TestFiles(unittest.TestCase):

    def test_get_logger(self):
        with tempfile.NamedTemporaryFile() as ftemp:
            logger = logging.get_logger('mylogger', fout=ftemp.name, level=orig_logging.CRITICAL,file_level=orig_logging.INFO)
            logger.info('foobar')
            with open(ftemp.name) as f:
                written = f.read()
                self.assertIn('mylogger - INFO - foobar', written)
