import unittest
import os
import tempfile
from vnlp.utils import files


class TestFiles(unittest.TestCase):

    sanity_check_url = 'https://gitlab.com/snippets/1726929/raw'
    sanity_check_content = 'Hello world!'

    def test_download(self):
        with tempfile.NamedTemporaryFile() as ftemp:
            with open(ftemp.name, 'wt') as f:
                f.write('dirty')
            # this should not write because file exists
            files.download(self.sanity_check_url, ftemp.name, skip_if_exists=True)
            with open(ftemp.name) as f:
                self.assertEquals('dirty', f.read())
            # this should overwrite
            files.download(self.sanity_check_url, ftemp.name, skip_if_exists=False)
            with open(ftemp.name) as f:
                self.assertEquals(self.sanity_check_content, f.read())
