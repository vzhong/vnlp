import os
import shutil
import unittest
import torch
import tempfile
import json
import logging
import numpy as np
from torch import nn
from torch.nn import functional as F
from vnlp.exp import tracker
from argparse import Namespace


class DummyModule(nn.Module):

    def __init__(self):
        super().__init__()
        self.lin = nn.Linear(2, 3)


class TestTracker(unittest.TestCase):

    def setUp(self):
        torch.manual_seed(0)
        np.random.seed(0)
        self.root = os.path.dirname(__file__)
        os.chdir(self.root)
        self.dexp = os.path.join(self.root, 'exp')
        self.args = Namespace(epoch=20, gpu=False, dexp=self.dexp, early_stop='dev_acc', name='myexp', module='mymodule', din=2, batch=2)
        self.dout = os.path.join(self.root, 'log')
        self.tracker = tracker.Tracker()
        if not os.path.isdir(self.dout):
            os.makedirs(self.dout)

    def tearDown(self):
        if os.path.isdir(self.dout):
            shutil.rmtree(self.dout)

    def test_is_best(self):
        t1 = {'dev': {'a': 1, 'b': 2}}
        t2 = {'dev': {'a': 3, 'b': 1}}
        t3 = {'dev': {'a': 2, 'b': 3}}
        self.assertTrue(self.tracker.is_best(t1, 'a', iteration=1, epoch=2))
        self.assertTrue(self.tracker.is_best(t2, 'a', iteration=2, epoch=2))
        self.assertFalse(self.tracker.is_best(t3, 'a', iteration=3, epoch=2))
        self.assertEquals([t1, t2, t3], self.tracker.log)
        self.assertEquals(self.tracker.iteration, 3)
        self.assertEquals(self.tracker.epoch, 2)
        self.assertEquals(t2, self.tracker.best('a'))

    def test_save_load(self):
        with tempfile.NamedTemporaryFile() as ftemp:
            tracker.Tracker(log=[1, 2, 3], epoch=10, iteration=5).save(ftemp.name)
            t = tracker.Tracker.load(ftemp.name)
            self.assertEquals([1, 2, 3], t.log)
            self.assertEquals(10, t.epoch)
            self.assertEquals(5, t.iteration)

    def test_save_data(self):
        data = {'foo': 'bar', 'a': 2}
        self.tracker.save_data(data, os.path.join(self.dout, 'tmp.json'), verbose=True)
        with open(os.path.join(self.dout, 'tmp.json')) as f:
            self.assertEquals(data, json.load(f))

    def test_find_checkpoints(self):
        fnames = ['myname.acc=0.4.pt', 'myname.acc=0.5.pt', 'myname.f1=0.6.pt']
        for fname in fnames:
            with open(os.path.join(self.dout, fname), 'wt') as f:
                f.write('')
        self.assertEquals(
            [(os.path.join(self.dout, 'myname.acc=0.5.pt'), 0.5), (os.path.join(self.dout, 'myname.acc=0.4.pt'), 0.4)],
            self.tracker.find_checkpoints(self.dout, early_stop='acc')
        )
        self.assertEquals(
            [(os.path.join(self.dout, 'myname.f1=0.6.pt'), 0.6)],
            self.tracker.find_checkpoints(self.dout, early_stop='f1')
        )
        self.assertEquals([], self.tracker.find_checkpoints(self.dout, early_stop='em'))

    def test_clean_old_checkpoints(self):
        fpaths = [os.path.join(self.dout, 'myname.metric={}.pt'.format(i)) for i in range(10)]
        fpaths2 = [os.path.join(self.dout, 'myname.foobar={}.pt'.format(i)) for i in range(10)]
        for fname in fpaths + fpaths2:
            with open(fname, 'wt') as f:
                f.write('')
        self.tracker.clean_old_checkpoints(self.dout, early_stop='metric', keep=4)
        for i in [6, 7, 8, 9]:
            self.assertTrue(os.path.isfile(os.path.join(self.dout, 'myname.metric={}.pt'.format(i))))
        for i in range(6):
            self.assertFalse(os.path.isfile(os.path.join(self.dout, 'myname.metric={}.pt'.format(i))))
        self.tracker.clean_old_checkpoints(self.dout, early_stop='metric', keep=2)
        for i in [8, 9]:
            self.assertTrue(os.path.isfile(os.path.join(self.dout, 'myname.metric={}.pt'.format(i))))
        for i in range(8):
            self.assertFalse(os.path.isfile(os.path.join(self.dout, 'myname.metric={}.pt'.format(i))))
        for i in range(10):
            self.assertTrue(os.path.isfile(os.path.join(self.dout, 'myname.foobar={}.pt'.format(i))))

    def test_checkpoint(self):
        module = DummyModule()
        optimizer = torch.optim.Adam(module.parameters())
        metrics = {'dev': {'acc': 100}}
        self.assertRaises(Exception, lambda: self.tracker.load_best_checkpoint(self.dout, early_stop='f1', module_root='modules'))
        self.tracker.save_checkpoint(self.dout, metrics, 'acc', module, optimizer, args=self.args, link_best=True)
        self.assertTrue(os.path.isfile(os.path.join(self.dout, 'checkpoint.best.pt')))
        state = self.tracker.load_best_checkpoint(self.dout, early_stop='acc')
        self.assertEqual(self.args, state['args'])
        module2 = DummyModule()
        module2.load_state_dict(state['module'])
        self.assertTrue(np.allclose(module.lin.weight.detach().numpy(), module2.lin.weight.detach().numpy()))
