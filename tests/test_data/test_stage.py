import unittest
from vnlp.data import stage
from vocab import Vocab


class TestTokenize(unittest.TestCase):

    def setUp(self):
        self.stage = stage.Tokenize()

    def test_call(self):
        ann = {'orig': 'I have a dream.'}
        backup = ann.copy()
        self.stage(ann)
        self.assertEquals(backup['orig'], ann['orig'])
        self.assertEquals([' I ', ' have ', ' a ', ' dream ', '. '], ann['tokens'])


class TestNumericalize(unittest.TestCase):

    def test_call(self):
        ann = {'tokens': [' I ', ' have ', ' a ', ' Dream ', '. ']}

        copy = ann.copy()
        s = stage.Numericalize(vocab=Vocab(), append_ends=False, train=True, lower=None)
        s(copy)
        self.assertEquals(['I', 'have', 'a', 'Dream', '.'], s.vocab.index2word(copy['num']))

        copy = ann.copy()
        s = stage.Numericalize(vocab=Vocab(), append_ends=True, train=True, lower=None)
        s(copy)
        self.assertEquals(['<S>', 'I', 'have', 'a', 'Dream', '.', '</S>'], s.vocab.index2word(copy['num']))

        copy = ann.copy()
        s = stage.Numericalize(vocab=Vocab(), append_ends=True, train=True, lower='all')
        s(copy)
        self.assertEquals(['<S>', 'i', 'have', 'a', 'dream', '.', '</S>'], s.vocab.index2word(copy['num']))

        copy = ann.copy()
        s = stage.Numericalize(vocab=Vocab(), append_ends=True, train=True, lower='first')
        s(copy)
        self.assertEquals(['<S>', 'i', 'have', 'a', 'Dream', '.', '</S>'], s.vocab.index2word(copy['num']))
