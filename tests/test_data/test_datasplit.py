import unittest
import numpy as np
from vnlp.data import datasplit


class TestDataSplit(unittest.TestCase):
    C = datasplit.DataSplit

    def setUp(self):
        np.random.seed(0)

    def test_batch(self):
        d = self.C([{'x': i} for i in range(5)])
        batches = list(d.batch(batch_size=2, shuffle=False, verbose=False))
        self.assertEquals(
            [[{'x': 0}, {'x': 1}], [{'x': 2}, {'x': 3}], [{'x': 4}]], batches
        )
        batches = list(d.batch(batch_size=2, shuffle=True, verbose=False))
        self.assertEquals(
            [[{'x': 2}, {'x': 0}], [{'x': 1}, {'x': 3}], [{'x': 4}]], batches
        )


class TestAnnotatedDataSplit(TestDataSplit):
    C = datasplit.AnnotatedDataSplit
