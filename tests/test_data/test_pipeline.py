import unittest
from vnlp.data import pipeline, dataset, datasplit, example, stage


class DummyStage(stage.Stage):

    def __call__(self, ann):
        ann['inc'] = ann.get('inc', ann['orig']) + 1


class TestPipeline(unittest.TestCase):

    def setUp(self):
        self.pipeline = pipeline.AnnotationPipeline(stages=[DummyStage(), DummyStage()])

    def test_call(self):
        ann = self.pipeline(0)
        self.assertEquals({'orig': 0, 'inc': 2}, ann)
