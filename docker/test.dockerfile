FROM python:3.6-jessie

RUN pip install http://download.pytorch.org/whl/cpu/torch-0.4.0-cp36-cp36m-linux_x86_64.whl

RUN mkdir -p /opt/work
WORKDIR /opt/work
COPY vnlp vnlp
COPY tests tests
COPY setup.py setup.py
COPY README.md README.md
