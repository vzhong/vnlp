#!/usr/bin/env bash

set -x
set -e

VERSION=$(python -c "import importlib; print(importlib.import_module('vnlp').__version__)")
IMAGE=registry.gitlab.com/vzhong/vnlp/test:$VERSION

docker build -t $IMAGE -f docker/test.dockerfile .
docker push $IMAGE

CONTAINER=vnlp_test

docker rm -f $CONTAINER
docker run -d -t --name $CONTAINER $IMAGE
docker exec $CONTAINER pip install -e ".[test]"
docker exec $CONTAINER nosetests tests --verbosity 3 --with-coverage --cover-erase --cover-package vnlp --cover-html
