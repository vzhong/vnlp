# VNLP

[![pipeline status](https://gitlab.com/vzhong/vnlp/badges/master/pipeline.svg)](https://gitlab.com/vzhong/vnlp/commits/master)

[![coverage report](https://gitlab.com/vzhong/vnlp/badges/master/coverage.svg)](https://gitlab.com/vzhong/vnlp/commits/master)


My machine learning and NLP library.


# Docker

build test image:

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/vzhong/vnlp:test .
docker push registry.gitlab.com/vzhong/vnlp
```
